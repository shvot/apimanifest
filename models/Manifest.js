var mongoose = require('mongoose');

var ManifestSchema = new mongoose.Schema({
  name: String,
  short_name: String,
  background_color: String,
  display: String,
  orientation: String,
  scope: String,
  start_url: String,
  theme_color: String,
});

module.exports = mongoose.model('Manifest', ManifestSchema);