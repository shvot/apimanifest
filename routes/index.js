var express = require('express');
var router = express.Router();
var manifest = require("../controllers/ManifestController.js");


router.get('/', function(req, res, next) {
  res.render('index', { title: 'Api Pwa' });
});


router.get('/create', function(req, res) {
  manifest.create(req, res);
});

router.get('/list', function(req, res) {
  manifest.list(req, res);
});

router.post('/save', function(req, res) {
  manifest.save(req, res);
});

router.get('/show/:id/manifest.json', (req, res)=> {manifest.show(req, res)});


router.get('/edit/:id', function(req, res) {
  manifest.edit(req, res);
});

router.post('/update/:id', function(req, res) {
  manifest.update(req, res);
});

router.post('/delete/:id', function(req, res, next) {
  manifest.delete(req, res);
});

module.exports = router;
