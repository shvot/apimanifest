const mongoose = require("mongoose");
const Manifest = require("../models/Manifest");

const manifestController = {};

/**
 * ROTAS DE VIEWS
 */

// Criar novo manifest
manifestController.create = function(req, res) {
    res.render("../views/manifest/create");
  };

//Listar manifests
manifestController.list = function(req, res) {
    Manifest.find({}).exec(function (err, manifests) {
        if (err) {
        console.log("Error:", err);
        }
        else {
        res.render("../views/manifest/index", {manifests: manifests});
        }
    });
};


// Editar Manifests
manifestController.edit = function(req, res) {
    Manifest.findOne({_id: req.params.id}).exec(function (err, manifest) {
      if (err) {
        console.log("Error:", err);
      }
      else {
        res.render("../views/manifest/edit", {manifest: manifest});
      }
    });
  };


/**
 * ROTAS DE API
 */


manifestController.save = async function(req, res, next) {

    var manifest = new Manifest(req.body);
  
   manifest
   .save(function(err) {
    if(err) {
      console.log(err);
      res.render("../views/manifest/create");
    } else {

        res.redirect("/show/" + manifest._id + "/manifest.json");
    }

    })
    
  };


  manifestController.delete = function(req, res) {
    Manifest.remove({_id: req.params.id}, function(err) {
      if(err) {
        console.log(err);
      }
      else {
        console.log("manifest deleted!");
        res.redirect("/list");
      }
    });
  };


  manifestController.update = function(req, res) {
    Manifest.findByIdAndUpdate(req.params.id, { $set: {
         name: req.body.name, 
         short_name: req.body.short_name, 
         theme_color: req.body.theme_color, 
         background_color: req.body.background_color, 
         scope: req.body.scope, 
         orientation: req.body.orientation, 
         display: req.body.display, 
         start_url: req.body.start_url 
        }}, { new: true }, function (err, manifest) {
      if (err) {
        console.log(err);
        res.render("../views/manifest/edit", {manifest: req.body});
      }
      res.redirect("/show/"+manifest._id+"/manifest.json");
    });
  };


  manifestController.show = function(req, res) {
    Manifest.findOne({_id: req.params.id}).exec(function (err, manifest) {
        if (err) {
          console.log("Error:", err);
        }
        else {
            let manifesto = {
                name: manifest.name,
                short_name: manifest.short_name,
                background_color: manifest.background_color,
                theme_color: manifest.theme_color,
                display: manifest.display,
                orientation: manifest.orientation,
                scope: manifest.scope,
                start_url:manifest.start_url,
                icons: [
                    {
                      src: "https://ezri.tk/assets/icons/icon-72x72.png",
                      sizes: "72x72",
                      type: "image/png"
                    },
                    {
                      src: "https://ezri.tk/assets/icons/icon-96x96.png",
                      sizes: "96x96",
                      type: "image/png"
                    },
                    {
                      src: "https://ezri.tk/assets/icons/icon-128x128.png",
                      sizes: "128x128",
                      type: "image/png"
                    },
                    {
                      src: "https://ezri.tk/assets/icons/icon-144x144.png",
                      sizes: "144x144",
                      type: "image/png"
                    },
                    {
                      src: "https://ezri.tk/assets/icons/icon-152x152.png",
                      sizes: "152x152",
                      type: "image/png"
                    },
                    {
                      src: "https://ezri.tk/assets/icons/icon-192x192.png",
                      sizes: "192x192",
                      type: "image/png"
                    },
                    {
                      src: "https://ezri.tk/assets/icons/icon-384x384.png",
                      sizes: "384x384",
                      type: "image/png"
                    },
                    {
                      src: "https://ezri.tk/assets/icons/icon-512x512.png",
                      sizes: "512x512",
                      type: "image/png"
                    }
                  ]
            };

            if (manifesto.orientation == ''){
                delete manifesto.orientation;
            }
            // console.log(manifesto);
          res.json(manifesto);
        }
      });
  };




module.exports = manifestController;